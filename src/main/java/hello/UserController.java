package hello;

import hello.domain.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import repos.UserRepos;

import java.util.Map;

@Controller
public class UserController {
    @Autowired
    private UserRepos userRepos;

    @GetMapping
    public String main(Map<String, Object> model) {
        Iterable<Users> users = userRepos.findAll();
        model.put("users", users);
        return "main";
    }
        @PostMapping
    public @ResponseBody String add(@RequestParam String first_name, @RequestParam String last_name,
               @RequestParam String login, @RequestParam String password, Map<String, Object> model) {
        Users user = new Users(first_name,last_name,login,password);
        userRepos.save(user);
        Iterable<Users> users = userRepos.findAll();
        model.put("users", users);
        return "main";
    }

    @PostMapping("remove")
    public String remove(@RequestParam String first_name, @RequestParam String last_name,
                      @RequestParam String login, @RequestParam String password, Map<String, Object> model) {
        Users user = new Users(first_name,last_name,login,password);
        userRepos.delete(user);
        Iterable<Users> users = userRepos.findAll();
        model.put("users", users);
        return "main";
    }
}