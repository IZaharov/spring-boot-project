package repos;

import hello.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepos extends JpaRepository<Users, Long> {
    List<Users> findAll(String login);
}